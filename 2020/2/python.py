import sys


def _db(path):
    with open(path) as f:
        for entry in f.read().splitlines():
            nm, char, password = entry.split(' ')
            n, m = map(int, nm.split('-'))
            yield (n, m, char.strip(':'), password)


def _is_count_within_range(n, m, char, password):
    return n <= password.count(char) <= m


def _is_positioned(n, m, char, password):
    return (password[n-1] == char) ^ (password[m-1] == char)


def _part1(db):
    return sum(_is_count_within_range(*entry) for entry in db)


def _part2(db):
    return sum(_is_positioned(*entry) for entry in db)


def main(db_path):
    print("part 1:", _part1(_db(db_path)))
    print("part 2:", _part2(_db(db_path)))


if __name__ == '__main__':
    sys.exit(main(sys.argv[1]))

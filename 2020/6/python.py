import sys


def _groups(path):
    with open(path) as f:
        for group in f.read().split("\n\n"):
            yield group.splitlines()


def _yes_answers(groups):
    for members in groups:
        yield set.union(*map(set, members))


def _common_answers(groups):
    for members in groups:
        yield set.intersection(*map(set, members))


def _count_answers(group_answers):
    return sum(len(answers) for answers in group_answers)


def _part1(path):
    return _count_answers(_yes_answers(_groups(path)))


def _part2(path):
    return _count_answers(_common_answers(_groups(path)))


def main(path):
    print("part 1:", _part1(path))
    print("part 2:", _part2(path))


if __name__ == '__main__':
    main(sys.argv[1])

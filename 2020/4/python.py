import sys
from string import hexdigits


def _read_passports(path):
    return [
        {k: f.split(":")[1] for f in line.split() if (k := f.split(":")[0]) != "cid"}
        for line in open(path).read().split("\n\n")
    ]


_field_validators = {
    'byr': lambda byr: 1920 <= int(byr) <= 2002,
    'iyr': lambda iyr: 2010 <= int(iyr) <= 2020,
    'eyr': lambda eyr: 2020 <= int(eyr) <= 2030,
    'hgt': lambda hgt:
        (hgt.endswith('cm') and 150 <= int(hgt.strip('cm')) <= 193) or
        (hgt.endswith('in') and 59 <= int(hgt.strip('in')) <= 76),
    'hcl': lambda hcl:
        hcl.startswith('#') and sum(c in hexdigits.lower() for c in hcl.lstrip('#')) == 6,
    'ecl': lambda ecl: ecl in ('amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'),
    'pid': lambda pid: len(pid) == 9 and pid.isdigit()
}


def _fields_valid(passport):
    return all(_field_validators[field](value) for field, value in passport.items())


def _part1(passports):
    return sum(len(p) == 7 for p in passports)


def _part2(passports):
    return sum(_fields_valid(p) for p in passports if len(p) == 7)


def main():
    passports = _read_passports(sys.argv[1])
    print("part 1:", _part1(passports))
    print("part 2:", _part2(passports))


if __name__ == '__main__':
    main()

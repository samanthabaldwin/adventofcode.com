import sys


def _grid(path):
    with open(path) as f:
        return f.read().splitlines()


def _trees(grid, right, down):
    return sum(1 for i, lineno in enumerate(range(0, len(grid), down))
               if grid[lineno][i*right % len(grid[lineno])] == '#' and lineno > 0)


def _part1(grid):
    return _trees(grid, 3, 1)


def _part2(grid):
    return \
        _trees(grid, 1, 1) * \
        _trees(grid, 3, 1) * \
        _trees(grid, 5, 1) * \
        _trees(grid, 7, 1) * \
        _trees(grid, 1, 2)


def main():
    grid = _grid(sys.argv[1])
    print("part 1:", _part1(grid))
    print("part 2:", _part2(grid))


if __name__ == '__main__':
    main()

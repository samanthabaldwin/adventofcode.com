import sys


def _data(path):
    return [int(number) for number in open(path).read().splitlines()]


def _is_sum_of(value, preamble):
    for n in preamble:
        if value - n in preamble:
            return True
    return False


def _part1(data, preamble_length):
    for i in range(preamble_length, len(data)):
        preamble = data[i-preamble_length:i]
        if not _is_sum_of(data[i], preamble):
            return data[i]


def _part2(data, preamble_length):
    invalid_number = _part1(data, preamble_length)
    data = data[:data.index(invalid_number)]
    for i in range(len(data)):
        acc = data[i]
        j = i + 1
        while acc < invalid_number:
            acc += data[j]
            j += 1
        if acc == invalid_number:
            return max(data[i:j]) + min(data[i:j])


def main(path):
    print("part 1:", _part1(_data(path), 25))
    print("part 2:", _part2(_data(path), 25))


if __name__ == '__main__':
    main(sys.argv[1])

import sys


def _instructions(path):
    return open(path).read().splitlines()


def emulate(instructions):
    visited = []
    acc = 0
    ip = 0
    while True:
        if ip >= len(instructions):
            return acc, ip
        if ip in visited:
            return acc, ip
        visited.append(ip)
        instr = instructions[ip]
        if instr.startswith("nop"):
            ip += 1
        if instr.startswith("acc"):
            acc += int(instr.split()[1])
            ip += 1
        if instr.startswith("jmp"):
            ip += int(instr.split()[1])


def mutate(instructions):
    for i, instr in enumerate(instructions):
        if instr.startswith("jmp"):
            replaced = [instr.replace("jmp", "nop")]
        elif instr.startswith("nop"):
            replaced = [instr.replace("nop", "jmp")]
        else:
            continue
        yield instructions[:i] + replaced + instructions[i+1:]


def _part1(path):
    return emulate(_instructions(path))[0]


def _part2(path):
    for instructions in mutate(_instructions(path)):
        acc, ip = emulate(instructions)
        if ip == len(instructions):
            return acc


def main(path):
    print("part 1:", _part1(path))
    print("part 2:", _part2(path))


if __name__ == '__main__':
    main(sys.argv[1])

import sys


def _seatids(path):
    with open(path) as f:
        for line in f.read().splitlines():
            row = int(line[:7].replace("F", "0").replace("B", "1"), 2)
            column = int(line[7:].replace("L", "0").replace("R", "1"), 2)
            yield (row * 8) + column


def _part1(seatids):
    return max(seatids)


def _part2(seatids):
    seatids = list(seatids)
    for seat in seatids:
        if seat + 2 in seatids and seat + 1 not in seatids:
            return seat + 1


def main():
    print("part 1:", _part1(_seatids(sys.argv[1])))
    print("part 2:", _part2(_seatids(sys.argv[1])))


if __name__ == '__main__':
    main()

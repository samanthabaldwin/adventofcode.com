import sys


def _groups(path):
    with open(path) as f:
        for rule in f.read().splitlines():
            yield rule.rstrip(".")


def held_by(rules, held_color, holding_colors):
    for rule in rules:
        holder, contains = rule.split(" bags contain")
        if held_color in contains:
            holding_colors.add(holder)
            held_by(rules, holder, holding_colors)
    return holding_colors


def holds(rules, holding_color):
    total = 0
    for rule in rules:
        holder, contains = rule.split(" bags contain ")
        if holding_color in holder:
            for held_bag in contains.split(", "):
                if held_bag.startswith("no"):
                    continue
                count = int(held_bag.split()[0])
                color = " ".join(held_bag.split()[1:3])
                total += count + (count * holds(rules, color))
    return total


def _part1(path):
    return len(held_by(list(_groups(path)), "shiny gold", set()))


def _part2(path):
    return holds(list(_groups(path)), "shiny gold")


def main(path):
    print("part 1:", _part1(path))
    print("part 2:", _part2(path))


if __name__ == '__main__':
    main(sys.argv[1])
